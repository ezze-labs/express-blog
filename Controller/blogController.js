import mongoose from "mongoose"
import Blog from "../Model/Blog.js"
import User from "../Model/User.js"

export const getAllBlogs = async (req, res, next) => {
    let blogs
    try {
        blogs = await Blog.find()
    } catch (err) {
        console.log(err)
    }
    if (!blogs) {
        return res
            .status(404)
            .json({ msg: 'No blog found' })
    } else {
        return res
            .status(200)
            .json(blogs)
    }
}

export const addBlog = async (req, res, next) => {
    const { title, description, image, user } = req.body
    let existingUser
    try {
        existingUser = await User.findById(user)
        if (!existingUser) {
            return res
                .status(400)
                .json({ msg: 'Cant find such user' })
        }
    } catch (err) {
        return console.log(err)
    }
    const blog = new Blog({
        title,
        description,
        image,
        user
    })
    try {
        
        const session = await mongoose.startSession()
        session.startTransaction()
        
        await blog.save({session})

        existingUser.blogs.push(blog)
        await existingUser.save({session})

        await session.commitTransaction()

        return res
            .status(201)
            .json(blog)
    } catch (err) {
        return console.log(err)
    }
}

export const updateBlog = async (req, res, next) => {
    const { title, description } = req.body
    const blogId = req.params.id
    try {
        const blog = await Blog.findByIdAndUpdate(blogId, {
            title,
            description
        })
        if (!blog) {
            return res
                .status(500)
                .json({ msg: 'Unable to update the blog.' })
        } else {
            return res
                .status(200)
                .json(blog)
        }
    } catch (err) {
        return console.log(err)
    }
}

export const getById = async (req, res, next) => {
    const id = req.params.id
    let blog
    try {
        blog = await Blog.findById(id)
        if (!blog) {
            return res
                .status(404)
                .json({ msg: 'No such blog found' })
        } else {
            return res
                .status(200)
                .json(blog)
        }
    } catch (err) {
        return console.log(err)
    }
}

export const deleteBlog = async (req, res, next) => {
    const id = req.params.id
    let blog
    try {
        
        blog = await Blog.findByIdAndDelete(id).populate('user')
        await blog.user.blogs.pull(blog)
        await blog.user.save()

        if (!blog) {
            return res
                .status(500)
                .json({ msg: 'No such blog found' })
        } else {
            return res
                .status(200)
                .json({ msg: 'Successfully deleted.' })
        }
    } catch (err) {
        return console.log(err)
    }
}

export const getByUserId = async (req, res, next) => {
    const userId = req.params.id
    let userBlogs
    try {
        userBlogs = await User.findById(userId).populate('blogs')
        if (!userBlogs) {
            return res
                .status(404)
                .json({ msg: 'No blogs found' })
        } else {
            return res
                .status(200)
                .json( userBlogs.blogs )
        }
    } catch (err) {
        return console.log(err)
    }
}
