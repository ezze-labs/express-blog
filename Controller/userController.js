import User from '../Model/User.js'
import bcrypt from 'bcryptjs'

export const getAllUsers = async (req, res, next) => {
    let users
    try {
        users = await User.find()
    } catch (err) {
        console.log(err)
    }
    if (!users) {
        return res
            .status(404)
            .json({ msg: 'No users found.' })
    } else {
        return res
            .status(200)
            .json({users})
    }
}

export const signup = async (req, res, next) => {
    const { name, email, password } = req.body
    let existingUser
    try {
        existingUser = await User.findOne({email})
    } catch (err) {
        return console.log(err)
    }
    if (existingUser) {
        return res
            .status(400)
            .json({msg: 'User already exists! Login instead.'})
    } else {
        const hashedPassword = bcrypt.hashSync(password)
        const user = new User({
            name,
            email,
            password: hashedPassword,
            blogs: [],
        })
        try {
            await user.save()
            return res
                .status(201)
                .json({ user })
        } catch (err) {
            return console.log(err)
        }
    }
}

export const login = async (req, res, next) => {
    const { email, password } = req.body
    let existingUser
    try {
        existingUser = await User.findOne({email})
    } catch (err) {
        return console.log(err)
    }
    if (!existingUser) {
        return res
            .status(400)
            .json({msg: 'User does not exist! Register instead.'})
    } else {
        const isPasswordCorrect = bcrypt.compareSync(password, existingUser.password)
        if (!isPasswordCorrect) {
            return res
                .status(400)
                .json({ msg: 'Incorrect password' })
        } else {
            return res
                .status(200)
                .json({ msg: 'Login Successfull' })
        }
    }
}