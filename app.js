
// npm i express mongoose dotenv bcryptjs

import express from 'express'
import mongoose from 'mongoose'
import userRouter from './Route/userRoute.js'
import blogRouter from './Route/blogRoute.js'

const app = express()

app.use(express.json())
app.use('/api/user', userRouter)
app.use('/api/blog', blogRouter)

// app.use('/api', (req, res, next) => {
//     res.send('Hello world')
// })

mongoose.connect(process.env.MONGO_URI)
.then( 
    () => 
    app.listen(process.env.PORT)     
)
.then(
    console.log('Connected to Database, and listening to port 3000')
)
.catch(
    (err) => console.log(err)
)

