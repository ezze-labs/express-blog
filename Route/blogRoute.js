import express from "express"
import { addBlog, deleteBlog, getAllBlogs, getById, getByUserId, updateBlog } from "../Controller/blogController.js"

const blogRouter = express.Router()

blogRouter.get('/', getAllBlogs)
blogRouter.post('/', addBlog)
blogRouter.put('/:id', updateBlog)
blogRouter.get('/:id', getById)
blogRouter.delete('/:id', deleteBlog)
blogRouter.get('/user/:id', getByUserId)

export default blogRouter
